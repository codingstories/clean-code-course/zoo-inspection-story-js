---
focus: src/ZooInspector.js
---
### The End

This story was created to show that:

* Introducing smaller functions can increase readability and changeability of a code
* Boolean parameters should not be used to manage execution flow of a method
* Sometimes a new class can dramatically decrease code complexity

Thank you for reading this story about the zoo inspection system. Good luck learning more principles of clean code and be brave to use them in your day-to-day work.
